var express = require('express');
var router = express.Router();
var pg = require('pg');
var os = require('os');

/* GET home page. */
router.get('/', function(req, res, next) {
    var client = new pg.Client('postgres://daxapp@45.79.199.237:5432/daxapp');
    var visitors = [];
    client.connect(function(err){
        client.query('SELECT * FROM visitors ORDER BY id DESC', function(err, result){
            var interfaces = os.networkInterfaces();
            var addresses = [];
            for (var k in interfaces) {
                for (var k2 in interfaces[k]) {
                    var address = interfaces[k][k2];
                    if (address.family === 'IPv4' && !address.internal) {
                        addresses.push(address.address);
                    }
                }
            }

            visitors = result.rows;
            res.render('index', { visitors: visitors, addresses: addresses.join(" | ") });
            client.end();
        })
    });
});

router.post('/', function(req, res, next) {
    var client = new pg.Client('postgres://daxapp@45.79.199.237:5432/daxapp');
    var visitors;
    client.connect(function(err){
        client.query('INSERT INTO visitors (name, visitdate) VALUES (\''+ req.body.name +'\', NOW());', function(err, result){
            if (err) {
                console.log(err);
            }

            client.end();
            res.redirect('/');
        });
    });
});

module.exports = router;
